import requests
from flask import Flask, render_template, request
from typing import Optional


app = Flask(__name__)


@app.route('/')
def index():
    response = None
    query = request.args.get('query')

    if query is not None:
        response = requests.post(url='http://141.105.64.131:8008/search_project', json={'query': query})
        if response.status_code == 200:
            response = response.json()['result']

    return render_template('index.html',
                           title='Витрина инновационных решений',
                           query=query,
                           response=response)

@app.route('/get_graph')
def get_graph():
    return render_template('graph.html')


@app.route('/projects/<project_id>')
def get_project(project_id: Optional[int]):
    title = ''
    description = ''
    effects = []
    tasks = []
    additional_materials = []

    if project_id is not None:
        response = requests.get(url=f'http://195.24.65.93:8080/projects/{project_id}')
        if response.status_code == 200:
            response = response.json()
            title = response['title']
            description = response['description']
            effects = response['effects']
            tasks = response['tasks']
            additional_materials = response['additional_materials']

    return render_template('single_project.html',
                           title=title,
                           description=description,
                           effects=effects,
                           additional_materials=additional_materials)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
