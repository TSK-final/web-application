FROM python:3.9-slim-buster

RUN apt update && \
    apt install --no-install-recommends -y build-essential gcc && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
    
WORKDIR /web-application

COPY requirements.txt /web-application/requirements.txt

RUN python3 -m venv venv
RUN venv/bin/python3 -m pip install --upgrade pip && \
    venv/bin/pip install --no-cache-dir -r requirements.txt

COPY . /web-application/

EXPOSE 5000
CMD ["venv/bin/python3", "app/main.py", "--host=0.0.0.0", "--port=5000"]

